build:
	nim c -d:release tag.nim

clean:
	rm tag

install:
	mkdir -p $(DESTDIR)/share/libgiraffe
	cp -r data/* $(DESTDIR)/share/libgiraffe/
	cp tag $(DESTDIR)/bin/tag

uninstall:
	rm -r $(DESTDIR)/share/libgiraffe
	rm $(DESTDIR)/bin/tag
