# tag

Text adventure giraffe game


A simple and loose implementation of dozens' [giraffe] game in a text encounter form.


## Install

- Install [Nim] ~= 1.6 and optionally, [make].

- Clone the repository, change into the directory and build:

```
git clone https://codeberg.org/43beans/tag
cd tag

# If make is installed
make
make install DESTDIR=/usr/local 

# Otherwise build with the nim command
# The tag executable and data/ directory can then be moved elsewhere
nim -c -d:release tag.nim
```


## Commands

```
-n, --name          Show a random giraffe name
-p, --play          Play game
-v, --version       Show game version
```

### In-game commands

- Show available commands: `h` or `help`
- Show places: `places`
- Go to a place: `go [place]`
- Do something (with dice): `do [action]`
- Exit game: `quit`


### Rule differences from the original game

- Encounters are randomly triggered by going to places.
- Multiple encounters with the same creature type are possible.
- Doing stuff with dice only happens during encounters.
- Doing stuff only works with one creature at a time.
- Creatures in encounters will disappear if the action was successful or there were too many attempts per encounter.


## Limitations

- No data save or load (single-session plays only)
- No place-specific encounters from the original game, e.g. Maraffe and Paraffe in giraffe home (all encounters are randomised)
- No option to switch between data sets (classic only)
- No command to eat the best leaves (game already ended)


## License and credits

Game code is released under the BSD-3-Clause license.

Classic data is derived from the [giraffe] TTRPG by dozens and made available under the original game's license.


[giraffe]: https://tilde.town/~dozens/giraffe/
[Nim]: https://nim-lang.org/
[make]: https://www.gnu.org/software/make
