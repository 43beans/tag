import std/[json, os, parseopt, random, rdstdin, strutils, wordwrap]
import libgiraffe


type
  Msg = tuple
    name: string
    text: string

  Script = seq[Msg]

  Key = tuple
    cmd: string
    binds: seq[string]

  Binds = seq[Key]


const
  env = (
    appName: "tag",
    appVer: "0.1",
  )


var
  fileScript = "script.json"
  fileBinds = "bindings.json"
  script: Script
  keyBinds: Binds
  termWidth = 72
  charaSetupOn = false
  playOn = false


# Load keybindings from a json file and return them as a sequence.
proc loadUIData(path = ""): void =
  var p = path
  if p == "":
    p = getPathData()
  keyBinds = to(parseFile(unixToNativePath(p & fileBinds)), Binds)
  script = to(parseFile(unixToNativePath(p & fileScript)), Script)


# Return the text of a script message given its name.
# If no script message text is found, return an empty string.
proc getScriptMsg*(name: string): string =
  for m in script:
    if m.name == name:
      return m.text
  return ""


# Replace variables in the game script with state values.
proc replaceVars*(str: string): string =
  var 
    place = lookupPlace(state.chara.placeName)
    resp = str
    pairs = [
      (v: "((action))", r: state.chara.lastDoAction),
      (v: "((adj))", r: state.chara.adj),
      (v: "((giraffe))", r: intToStr(state.chara.giraffe)),
      (v: "((name))", r: state.chara.name),
      (v: "((places))", r: join(getPlaceNames(), ", ")),
      (v: "((placeDesc))", r: place.desc),
      (v: "((placeName))", r: state.chara.placeName),
    ]
  for p in pairs:
    if p.v in str:
      resp = replace(resp, p.v, p.r)
  return resp


# Replace the entity variable in the game script with state values and return
# multiple entity information strings in a sequence.
proc replaceEntityVar*(str: string): seq[string] =
  var seqStr: seq[string]
  for e in state.enc.ents:
    add(seqStr, replace(str, "((entity))", join([e.name, " ", e.status,
      e.comp])))
  return seqStr
# Show readline prompt with a prefix string and return user string input.
proc prompt(ps = ">> "): string =
  return readLineFromStdin(ps)



# Output string to stdout. If `elb` is set to true, it will insert an extra
# line break after the text. If `repl` is set to true, it will call
# `replaceVars()` to replace variables in the string.
proc msg(str: string, elb = false, repl = false): void =
  if repl:
    echo wrapWords(replaceVars(str), termWidth, true)
  else:
    echo wrapWords(str, termWidth, true)
  if elb:
    echo ""


# Return the command corresponding to the user key input based on keybindings
# data. If no command is found, return an empty string.
proc lookupCmd(str: string): string =
  for k in keyBinds:
    if (str in k.binds) or (split(str)[0] in k.binds):
      return k.cmd
  return ""


# Lookup a keybinding by command string and return the Key.
# Return a Key with empty key values if no matching Key is found.
proc lookupKey(cmd: string): Key =
  for k in keyBinds:
    if k.cmd == cmd:
      return k
  return (cmd: cmd, binds: @[])


# Output app version. If `elb` is set to true, it will insert an extra line
# break after the text.
proc handleShowVersion(elb = false): void =
  msg(join([env.appName, " ", env.appVer, " :: ",
    getScriptMsg("cmdVersionAppDesc")]), elb)


# Output place info and entity info (if there is an encounter) to stdout.
proc handleLook(): void =
  var place = lookupPlace(state.chara.placeName)
  if place.desc != "":
    msg(getScriptMsg("cmdLookPlaceName"), false, true)
    msg(getScriptMsg("cmdLookPlaceDesc"), true, true)
  else:
    msg(getScriptMsg("cmdLookPlaceName"), true, true)
  if len(state.enc.ents) > 0:
    var
      entsStr = replaceEntityVar(getScriptMsg("cmdLookEnt"))
      entCount = 0
    # Add line break at the last entity
    for e in entsStr:
      if len(entsStr) == 1:
        msg(e, true)
      elif entCount < (len(entsStr) - 1):
        msg(e, false)
        entCount += 1
      else:
        msg(e, true)


# Handle place navigation and trigger a new encounter.
proc handleGo(input: string): void =
  var 
    newPlaceName = input
    cmdLen: int
  # No place name given
  if len(split(input)) == 1:
    msg(getScriptMsg("errCmdGoNoPlace"), true)
    return
  # Extract the place name
  else:
    cmdLen = len(split(input)[0]) + 1
    newPlaceName = input[cmdLen..^1]
  # Don't switch places if the requested place is the same
  if newPlaceName == state.chara.placeName:
    msg(getScriptMsg("errCmdGoSamePlace"), true)
    return
  # Don't switch places if the requested place doesn't exist
  elif lookupPlace(newPlaceName).name == "":
    # Temporarily set place name to show message then revert it
    var lastPlace = state.chara.placeName
    state.chara.placeName = newPlaceName
    msg(getScriptMsg("errCmdGoPlaceNotFound"), true, true)
    state.chara.placeName = lastPlace
    return
  else:
    state.chara.placeName = newPlaceName

  # Reset or trigger new encounter 
  state.enc = randEncounter()
  handleLook()


# Handle do action and showing action outcome.
proc handleDo(input: string): void =
  var
    cmdLen: int
  # No action given
  if len(split(input)) == 1:
    msg(getScriptMsg("errCmdDoNoAction"), true)
    return
  # Extract the action substring
  else:
    cmdLen = len(split(input)[0]) + 1
    state.chara.lastDoAction = input[cmdLen..^1]
  # Do actions only when there is an encounter with entities
  state.enc.doTries += 1
  if len(state.enc.ents) > 0 and not hasExceedTries().res:
    var oc = doAction(state.chara.lastDoAction)
    if oc.isLikeGir:
      msg(getScriptMsg("cmdDoLikeGiraffe"), false, true)
    else:
      msg(getScriptMsg("cmdDoLikeHuman"), false, true)
    # Notify with outcome
    case oc.eval
    of "exceptional success":
      msg(getScriptMsg("cmdDoExcSuccess"))
    of "success":
      msg(getScriptMsg("cmdDoSuccess"))
    of "exceptional failure":
      msg(getScriptMsg("cmdDoExcFailure"))
    of "failure":
      msg(getScriptMsg("cmdDoFailure"))
    of "giraffe":
      msg(getScriptMsg("cmdDoGiraffe"))
    msg(getScriptMsg("attrGiraffe"), true, true)
    # Check whether the game ended
    if state.ending.isEnd:
      if state.ending.eval == "giraffe":
        msg(getScriptMsg("endGiraffe"), true)
      else:
        msg(getScriptMsg("endHuman"), true)
      playOn = false

  # No encounter
  else:
    msg(getScriptMsg("errCmdDoNoEnc"), true, true)


# Set up a new character.
proc setupChara(): void =
  while charaSetupOn:
    state.chara.name = randName()
    state.chara.adj = randAdj()

    msg(getScriptMsg("promptName"), false, true)
    var input = prompt("")
    if input != "":
      state.chara.name = input
    
    msg(getScriptMsg("promptAdj"), false, true)
    input = prompt("")
    if input != "":
      state.chara.adj = input
    
    msg(getScriptMsg("confCharaInfo"), false, true)
    input = prompt("")
    var key = lookupKey("charaConfirm")
    for b in key.binds:
      if input == b:
        msg(getScriptMsg("confCharaCreate"), true)
        charaSetupOn = false


# Prompt for player input and direct commands to handlers.
proc startPlay(): void =
  var
    input: string
    cmd: string
  while playOn:
    input = prompt()
    cmd = lookupCmd(input)
    state.chara.lastAction = input

    if cmd == "help":
      msg(getScriptMsg("cmdHelp"), true)

    elif cmd == "giraffe":
      msg(state.chara.name & " (" & state.chara.adj & ")")
      msg(getScriptMsg("attrGiraffe"), true, true)

    elif cmd == "look":
      handleLook()

    elif cmd == "places":
      msg(getScriptMsg("cmdPlaces"), true, true)

    elif cmd == "quit":
      msg(getScriptMsg("cmdQuit"), true)
      playOn = false 
      return

    elif cmd == "version":
      handleShowVersion(true)
  
    elif cmd == "go":
      handleGo(input)
  
    elif cmd == "do":
      handleDo(input)

    else:
      var resp = sample([getScriptMsg("cmdOtherAction"),
        getScriptMsg("cmdUnknown")])
      msg(resp, true, true)


# Run the game routine.
proc runGame(): void =
  msg(getScriptMsg("gameIntro"), true)

  charaSetupOn = true 
  setupChara()

  playOn = true
  startPlay()


# Parse run flags from the command line.
proc run(): void =
  initDefState()
  loadData()
  loadUIData()
  var 
    p = initOptParser(quoteShellCommand(commandLineParams()))
    arg: string
    hasFlag = false
    flag: string
    cmd: string

  for kind, k, v in p.getopt():
    case kind
    of cmdArgument:
      arg = k

    of cmdShortOption, cmdLongOption:
      # Prepend hyphens to distinguish from in-game commands
      if len(k) == 1:
        flag = "-" & k
      else:
        flag = "--" & k
      cmd = lookupCmd(flag)

      if cmd == "cliHelp":
        msg(getScriptMsg("cliHelp"))
        hasFlag = true

      elif cmd == "cliName":
        state.chara.name = randName()
        msg(getScriptMsg("cliName"), false, true)
        hasFlag = true

      elif cmd == "cliPlay":
        runGame()
        hasFlag = true

      elif cmd == "cliVersion":
        handleShowVersion()
        hasFlag = true

      else:
        msg(getScriptMsg("errCliUnknownCmd"))
        msg(getScriptMsg("cliHelp"))
        hasFlag = true

    of cmdEnd:
      discard

  # Run game if no arguments or command flags were given
  if arg == "" and not hasFlag:
    runGame()


run()
