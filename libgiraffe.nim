import std/[json, jsonutils, os, random, strutils, unicode]


type
  Ent* = tuple
    name: string
    status: string
    comp: string

  Place* = tuple
    name: string
    desc: string
    entTypes: seq[string]

  Enc* = tuple
    place: Place
    ents: seq[Ent]
    doTries: int
  
  Chara* = tuple
    name: string
    adj: string
    giraffe: int
    placeName: string
    lastAction: string
    lastDoAction: string
    lang: string
    gameMode: string

  Dice* = tuple
    min: int
    max: int

  Outcome* = tuple
    eval: string
    roll: seq[int]
    bestRoll: int
    isLikeGir: bool
    hasAdj: bool
    evolve: int

  End* = tuple
    isEnd: bool
    eval: string

  Names* = tuple
    adjs: seq[string]
    names: seq[string]
    nouns: seq[string]

  Ents* = tuple
    names: seq[string]
    statuses: seq[string]
    comps: seq[string]

  Places* = seq[Place]

  DoCrit* = tuple
    isWords: seq[string]
    notWords: seq[string]
    giraffes: seq[string]
    humans: seq[string]

  # Game data
  Data* = tuple
    names: Names
    ents: Ents
    places: Places
    doCrit: DoCrit

  # Game state
  State* = tuple
    chara: Chara
    pathsData: seq[string]
    pathsPlayerData: seq[string]
    dice: Dice
    maxEnts: int
    pctEnc: int
    maxEncRetries: int
    enc: Enc
    ending: End

const
  env = (
    libName: "libgiraffe",
    libVer: 0.1,
    fileNames: "names.json",
    fileEnts: "entities.json",
    filePlaces: "places.json",
    fileDoCrit: "docrit.json",
  )


var
  data*: Data
  state*: State


# Initialise a new default game state.
proc initDefState*(): void =
  state.chara = (
    name: "giraffe",
    adj: "horsey",
    giraffe: 3,
    placeName: "giraffe home",
    lastAction: "",
    lastDoAction: "",
    lang: "en",
    gameMode: "classic"
  )
  var relPathData = join([state.chara.lang, "/", state.chara.gameMode, "/"])
  state.pathsData = @[
    "/usr/share/" & env.libName & "/" & relPathData,
    "/usr/local/share/" & env.libName & "/" & relPathData,
    "data/" & relPathData,
  ]
  state.pathsPlayerData = @["savedata/"]
  state.dice = (min: 1, max: 6)
  state.maxEnts = 3
  state.pctEnc = 60
  state.maxEncRetries = 1
  state.enc = (
    place: (name: "", desc: "", entTypes: @[]),
    ents: @[],
    doTries: 0,
  )
  state.ending = (isEnd: false, eval: "giraffe")


# Check a sequence of preset paths for the data files and return the first path
# containing data as a string.
proc getPathData*(seqPaths = state.pathsData, file = env.fileNames): string =
  for p in seqPaths:
    if fileExists(unixToNativePath(p & file)):
      return p


# Load the data tuple according to the play mode.
proc loadData*(path = ""): void =
  var p = path
  if p == "":
    p = getPathData()
  # Load JsonNodes from files into a tuple
  data.names = to(parseFile(unixToNativePath(p & env.fileNames)), Names)
  data.ents = to(parseFile(unixToNativePath(p & env.fileEnts)), Ents)
  data.places = to(parseFile(unixToNativePath(p & env.filePlaces)), Places)
  data.doCrit = to(parseFile(unixToNativePath(p & env.fileDoCrit)), DoCrit)
  # Load randomiser
  randomize()


# Load game state from a json file.
proc loadPlayerData*(path = "", file: string): void =
  var p = path
  if path == "":
    p = getPathData(state.pathsPlayerData, file)
  state = to(parseFile(unixToNativePath(p & file)), State)


# Save game state to a json file.
proc savePlayerData*(path = state.pathsPlayerData[^1], file: string): void =
  var
    node = pretty(toJson(state))
    fileSave = replace(file, " ", "-")
  if not dirExists(unixToNativePath(path)):
    createDir(unixToNativePath(path))
  if fileSave == "":
    fileSave = replace(toLower(state.chara.name), " ", "-")  & ".json"
  writeFile(unixToNativePath(path & fileSave), node)


# Return a sequence of place names.
proc getPlaceNames*(): seq[string] =
  var places: seq[string]
  for p in data.places:
    add(places, p.name)
  return places


# Return place details given the place name. If no place is found, return a
# Place with empty key values.
proc lookupPlace*(str: string): Place =
  for p in data.places:
    if p.name == str:
      return p
  return (name: "", desc: "", entTypes: @[])


# Determine to which entity in an encounter an action string applies by
# matching the entity name. If no entity is found in the string, there are
# multiple entities in an encounter and `randomness` is true, return a random
# entity in the encounter, or the first entity if `randomness` is false.
proc getTargetEntity*(str: string, randomness = true): string =
  var 
    wSplit: seq[string]
    entName: string
  for t in state.enc.ents:
    wSplit = split(t.name, " ")
    entName = wSplit[len(wSplit) - 1]
    if (" " & entName & " ") in str or endsWith(str, " " & entName):
      return t.name
  if randomness:
    return sample(state.enc.ents).name
  else:
    return state.enc.ents[0].name


# Determine whether an action string is giraffe-like or human-like by matching
# association keywords. Return true if giraffe-like or false if human-like.
# If `randomness` is true, randomly return a boolean value if no keywords are
# found.
proc isLikeGiraffe*(str: string, randomness = true): bool =
  for g in data.doCrit.giraffes:
    if continuesWith(str, g, 0) or endsWith(str, g):
      for w in data.doCrit.isWords:
        if startsWith(str, w & " " & g) or endsWith(str, w & " " & g):
          return true
      for w in data.doCrit.notWords:
        if endsWith(str, w & " " & g) or endsWith(str, w & " " & g):
          return false
  for h in data.doCrit.humans:
    if continuesWith(str, h, 0) or endsWith(str, h):
      for w in data.doCrit.notWords:
        if startsWith(str, w & " " & h) or endsWith(str, w & " " & h):
          return true
      for w in data.doCrit.isWords:
        if startsWith(str, w & " " & h) or endsWith(str, w & " " & h):
          return false
  # Ends in human but does not match keywords, ends in giraffe but does not
  # match keywords, or no keywords found
  if randomness:
    return sample([true, false])
  else:
    return false


# Check whether a string contains the character adjective.
# Return if true and false otherwise.
proc hasAdj*(str: string): bool =
  if (count(str, " " & state.chara.adj & " ") > 0) or
    (count(str, " " & state.chara.adj) > 0):
    return true
  else:
    return false


# Check whether the character has exceeded the maximum number of "do action"
# attempts during an encounter. Return a tuple containing a boolean (true if
# exceeded, false otherwise) and the resulting difference.
proc hasExceedTries*(): tuple[res: bool, diff: int] =
  if state.enc.doTries > (len(state.enc.ents) +
    (len(state.enc.ents) * state.maxEncRetries)):
    return (res: true, diff: state.enc.doTries - (len(state.enc.ents) +
      (len(state.enc.ents) * state.maxEncRetries)))
  else:
    return (res: false, diff: (len(state.enc.ents) +
      (len(state.enc.ents) * state.maxEncRetries)) - state.enc.doTries)


# Return a sequence of random integers within a given max value.
proc rollDice*(num = 1, dice = state.dice.max): seq[int] =
  var roll: seq[int]
  for n in 1..num:
    add(roll, rand(1..dice))
  return roll


# Translate percent probability to boolean. For example, if the percentage is
# set to 20, the function returns true roughly 20% of the time.
# This only supports whole percentage points. If the percentage is less than 1
# or greater than 100, return false.
proc pctToBool*(pct: int): bool =
  var randInt = rand(1..100)
  if (pct >= 1) and (pct <= 100) and (randInt <= pct):
    return true
  else:
    return false


# Return a random giraffe name based on the name data.
proc randName*(): string =
  return join(@[
    capitalize(sample(data.names.adjs)), " ",
    capitalize(sample(data.names.names)), " ",
    capitalize(sample(data.names.adjs)), " ",
    capitalize(sample(data.names.nouns))
  ])


# Return a random adjective based on the name data.
proc randAdj*(): string =
  return capitalize(sample(data.names.adjs))


# Return a random place based on the place data.
proc randPlace*(): Place =
  return data.places[rand(0..(len(data.places) - 1))]


# Return a random sequence of entities based on the entities data and the
# character's current location. If the location has no entity types or the
# probability boolean is false, return an empty sequence.
proc randEntities*(): seq[Ent] =
  var 
    place = lookupPlace(state.chara.placeName)
    maxEnts = rand(1..state.maxEnts)
    entTypes: seq[string]
    ents: seq[Ent]
  # All entity types
  if (("all" in place.entTypes) or ("any" in place.entTypes)) and
    pctToBool(state.pctEnc):
    for n in 1..maxEnts:
      add(ents, (name: sample(data.ents.names),
        status: sample(data.ents.statuses), comp: sample(data.ents.comps)))
  # Select entity types
  elif len(place.entTypes) > 0 and pctToBool(state.pctEnc):
    for n in 1..maxEnts:
      add(entTypes, sample(place.entTypes))
    # Look up the entity type in the entities data, which includes noun
    # articles with the entity name
    for t in entTypes:
      for e in data.ents.names:
        if endsWith(e, t):
          add(ents, (name: e, status: sample(data.ents.statuses),
            comp: sample(data.ents.comps)))
  return ents


# Return a random encounter based on the character's current location.
proc randEncounter*(): Enc =
  return (place: lookupPlace(state.chara.placeName), ents: randEntities(),
    doTries: 0)


# Return a random outcome.
proc randOutcome*(gir: int, isLikeGir: bool, hasAdj: bool): Outcome =
  var 
    roll = rollDice(state.dice.min, state.dice.max)
    bestRoll = roll[0]
    eval = "failure"
    evolve = 0
  if hasAdj and isLikeGir:
    roll = rollDice(2, state.dice.max)
    bestRoll = max(roll[0], roll[1])
  elif hasAdj and not isLikeGir:
    roll = rollDice(2, state.dice.max)
    bestRoll = min(roll[0], roll[1])
  if bestRoll == gir:
    eval = "giraffe"
  elif (isLikeGir and bestRoll == state.dice.min) or
    (not isLikeGir and bestRoll == state.dice.max):
    eval = "exceptional success"
    evolve = 1
  elif (isLikeGir and bestRoll < gir) or
    (not isLikeGir and bestRoll > gir):
    eval = "success"
  elif (isLikeGir and bestRoll == state.dice.max) or
    (not isLikeGir and bestRoll == state.dice.min):
    eval = "exceptional failure"
    evolve = -1
  return (eval: eval, roll: roll, bestRoll: bestRoll, isLikeGir: isLikeGir,
    hasAdj: hasAdj, evolve: evolve)


# Reset the encounter state.
proc resetEncounter*(): void =
  state.enc = (
    place: (name: "", desc: "", entTypes: @[]),
    ents: @[],
    doTries: 0,
  )


# Check whether the giraffe value has reached min or max and return the status.
proc isGameEnd*(gir: int): End =
  var
    isEnd = false
    eval: string
  if gir == state.dice.max:
    isEnd = true
    eval = "giraffe"
  elif gir == state.dice.min:
    isEnd = true
    eval = "human"
  elif gir < toInt((state.dice.max / 2)):
    eval = "human-like"
  else:
    eval = "giraffe-like"
  return (isEnd: isEnd, eval: eval)


# Generate an outcome for an action string and update the character giraffe
# attribute.
proc doAction*(str: string): Outcome =
  var 
    oc = randOutcome(state.chara.giraffe, isLikeGiraffe(str), hasAdj(str))
    tgtEnt = getTargetEntity(str)
    encEnts = state.enc.ents
  # Remove target entity if outcome is exceptional success or success
  case oc.eval
  of "exceptional success", "success":
    for n in 0..(len(encEnts) - 1):
      if endsWith(encEnts[n].name, tgtEnt):
        del(state.enc.ents, n)
        break
  # Update giraffe attribute
  state.chara.giraffe += oc.evolve
  state.ending = isGameEnd(state.chara.giraffe)
  # Reset encounter if max tries reached or there are no entities remaining
  if (not hasExceedTries().res and hasExceedTries().diff == 0) or
    len(state.enc.ents) == 0:
    resetEncounter()
  return oc


# Set game state for a new game.
proc initGame*(name: string, adj: string, mode: string): void =
  state.chara.name = name
  state.chara.adj = adj
  state.chara.gameMode = mode
  state.ending = (isEnd: false, eval: "giraffe")
